/* code credits: 
Ben Cox 2018 from code referenced below:

YourDuino.com Example Software Sketch for I2C LCD: terry@yourduino.com 

 deba168,INDIA http://www.instructables.com/id/DIY-Arduino-Battery-Capacity-Tester-V10-/
*/

#include <Wire.h>                 // i2c comms
#include <RTClib.h>               // real time clock

PCF8563 rtc;

#include <LiquidCrystal_I2C.h>    // for the 1602 display

// set the LCD address to 0x27 for a 16 chars 2 line display
LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

// define the IO pins

#define Fan_Pin 5
#define Buzzer_Pin 6
#define Relay1_Pin 9
#define Relay2_Pin 10
#define Relay3_Pin 11
#define Relay4_Pin 12
#define LedPin 13

#define Bat_Pin A0
#define Res_Pin A1

  
float Capacity = 0.0; // Capacity in mAh
float Res_Value = 4.7;  // Resistor Value in Ohm
float Vcc = 4.05; // Voltage of Arduino 5V pin ( Mesured by Multimeter )
float Current = 0.0; // Current in Amp
float mA=0;         // Current in mA
float Bat_Volt = 0.0;  // Battery Voltage 
float Res_Volt = 0.0;  // Voltage at lower end of the Resistor 
float Bat_High = 4.2; // Battery High Voltage
float Bat_Low = 2.9; // Discharge Cut Off Voltage
unsigned long previousMillis = 0; // Previous time in ms
unsigned long millisPassed = 0;  // Current time in ms
float sample1 =0;
float sample2= 0;
int x = 0;
int row = 0;
int Fan_Speed;    // PWM control



//************************ LCD Display Draw Function *******************************************************
void draw(void) {
   
   if ( Bat_Volt < 1){
    lcd.clear();
    lcd.setCursor(2,0);        // set position
    lcd.print("No Battery!");
    Fan_Speed = 0;
   }
   else if ( Bat_Volt > Bat_High){
    lcd.clear();
    lcd.setCursor(3,0);        // set position
    lcd.print("High-V!"); 
    Fan_Speed = 0;
   }
   else if(Bat_Volt < Bat_Low){
    lcd.clear();
    lcd.setCursor(5,0);        // set position
    lcd.print("Low-V!");
    Fan_Speed = 0; 
   }
   else if(Bat_Volt >= Bat_Low && Bat_Volt < Bat_High  ){
     lcd.clear();
     lcd.setCursor(0, 0);         // Line 1 pos 1
     lcd.print("V:");   
     lcd.print(Bat_Volt);         // display Battery Voltage in Volt
     
     lcd.setCursor(0, 1);         // Line 2 pos 1
     lcd.print("I:");
     lcd.print(mA);               // display current in mA
     
     lcd.setCursor(10, 0);         // Line 1 pos 2 
     lcd.print("mAh:");
     lcd.setCursor(10, 1);         // Line 1 pos 2 
     lcd.print(Capacity);     // display capacity in mAh
     
     //Fan_Speed = map(mA, 600, 800, 128, 255);
     if (mA -600 > 255){
        Fan_Speed = 255;
     }
     else {
        Fan_Speed = mA - 600;   
        
      
     }
   }
}


//******************************Buzzer Beep Function *********************************************************

  void beep(unsigned char delay_time){
  analogWrite(Buzzer_Pin, 20);      // PWM signal to generate beep tone
  delay(delay_time);          // wait for a delayms ms
  analogWrite(Buzzer_Pin, 0);  // 0 turns it off
  delay(delay_time);          // wait for a delayms ms  

} 

void test_relays() {
    
    digitalWrite(Relay1_Pin, HIGH);   // Turn On the relay 
    // check for a battery
    delay(500);
    digitalWrite(Relay1_Pin, LOW);    // Turn Off the relay

    digitalWrite(Relay2_Pin, HIGH);   // Turn On the relay 
    // check for a battery
    delay(500);
    digitalWrite(Relay2_Pin, LOW);    // Turn Off the relay

    digitalWrite(Relay3_Pin, HIGH);   // Turn On the relay 
    // check for a battery
    delay(500);
    digitalWrite(Relay3_Pin, LOW);    // Turn Off the relay

    digitalWrite(Relay4_Pin, HIGH);   // Turn On the relay 
    // check for a battery
    delay(500);
    digitalWrite(Relay4_Pin, LOW);    // Turn Off the relay
    

  
}


//*******************************Setup Function ***************************************************************
  
  void setup() {
    Serial.begin(9600);
    
    //--------- Prepare the pins ----------------

    rtc.begin();
    if (! rtc.isrunning()) {
      Serial.println("RTC is NOT running!");
      // following line sets the RTC to the date & time this sketch was compiled
      rtc.adjust(DateTime(__DATE__, __TIME__));
    }
    
    
    pinMode(LedPin, OUTPUT);       // Prepare the on-board LED
    pinMode(Relay1_Pin, OUTPUT);
    pinMode(Relay2_Pin, OUTPUT);
    pinMode(Relay3_Pin, OUTPUT);
    pinMode(Relay4_Pin, OUTPUT);
    pinMode(Buzzer_Pin, OUTPUT);
    pinMode(Fan_Pin, OUTPUT);
    
    digitalWrite(Relay1_Pin, LOW);  // Relays off during setup
    digitalWrite(Relay2_Pin, LOW);  // Relays off during setup
    digitalWrite(Relay3_Pin, LOW);  // Relays off during setup
    digitalWrite(Relay4_Pin, LOW);  // Relays off during setup

    
    Serial.println("CLEAR DATA");
    Serial.println("LABEL,Time,Bat_Volt,capacity");
    
    //Serial.println("Arduino Battery Capacity Tester v1.0");
    //Serial.println("BattVolt Current mAh");
  

    lcd.init();                      // initialize the lcd 
    lcd.backlight();
    //-------- Write characters on the display ------------------
    
    DateTime now = rtc.now();
    //char buf[100];
    //strncpy(buf,"DD.MM.YYYY  hh:mm:ss\0",100);
    //Serial.println(now.format(buf));
    
    lcd.setCursor(6,0);
    lcd.print(now.day(), DEC);
    lcd.print("-");
    lcd.print(now.month(), DEC);
    lcd.print("-");
    lcd.print(now.year(), DEC);
    lcd.setCursor(6,1);
    lcd.print(now.hour(), DEC);
    lcd.print(":");
    lcd.print(now.minute(), DEC);
    lcd.print(":");
    lcd.print(now.second(), DEC);
    delay(5000);

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("test");
    lcd.setCursor(0,1);
    lcd.print("relays");
    test_relays();
    delay(1800); 
    lcd.clear(); 

    lcd.setCursor(0,0);
    lcd.print("test");
    lcd.setCursor(0,1);
    lcd.print("fan");

    
    
    analogWrite(Fan_Pin, 255);      // PWM signal to set fan speed
    lcd.setCursor(5,1);
    lcd.print("255");
    delay(5000);          // wait for a delayms ms
    
    analogWrite(Fan_Pin, 0);  // 0 turns it off

    delay(1800); 
    lcd.clear(); 

    

}

  //********************************Main Loop Function***********************************************************
  void loop() {
 // Vcc = readVcc()/1000.0; // Conevrrt mV to Volt

  
  // Voltage devider Out = Bat_Volt * R2/(R1+R2 ) // R1 =10K and R2 =10K 
  
  //************ Measuring Battery Voltage ***********
  
  for(int i=0;i< 100;i++)
  {
   sample1=sample1+analogRead(Bat_Pin); //read the voltage from the divider circuit
   delay (2);
  }
  sample1=sample1/100; 
  Bat_Volt = 2* sample1 * Vcc/ 1024.0; 

  // *********  Measuring Resistor Voltage ***********

   for(int i=0;i< 100;i++)
  {
   sample2=sample2+analogRead(Res_Pin); //read the voltage from the divider circuit
   delay (2);
  }
  sample2=sample2/100;
  Res_Volt = 2* sample2 * Vcc/ 1024.0;

  //********************* Checking the different conditions *************
  
  if ( Bat_Volt > Bat_High){
    digitalWrite(Relay1_Pin, LOW); // Turned Off the MOSFET // No discharge 
    beep(200);
    Serial.println( "Warning High-V! ");
    delay(1000);
   }
   
   else if(Bat_Volt < Bat_Low){
      digitalWrite(Relay1_Pin, LOW);
      beep(200);
      Serial.println( "Warning Low-V! ");
      delay(1000);
  }
  else if(Bat_Volt > Bat_Low && Bat_Volt < Bat_High  ) { // Check if the battery voltage is within the safe limit
      digitalWrite(Relay1_Pin, HIGH);
      millisPassed = millis() - previousMillis;
      Current = (Bat_Volt - Res_Volt) / Res_Value;
      mA = Current * 1000.0 ;
      Capacity = Capacity + mA * (millisPassed / 3600000.0); // 1 Hour = 3600000ms
      previousMillis = millis();
      Serial.print("DATA,TIME,"); Serial.print(Bat_Volt); Serial.print(","); Serial.println(Capacity);
      row++;
      x++;
      delay(4000); 
 
     }
  
   //**************************************************

    draw();
    analogWrite(Fan_Pin, Fan_Speed);      // PWM signal to set fan speed

  
 //*************************************************
 }    